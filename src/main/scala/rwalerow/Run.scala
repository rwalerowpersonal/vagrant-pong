package rwalerow

import akka.actor.{Actor, ActorSystem, Props}

case object PingMessage
case object PongMessage
case object StartMessage
case object StopMessage

class Pong extends Actor {
  def receive = {
    case PingMessage =>
      println("  pong")
      sender ! PongMessage
    case StopMessage =>
      println("pong stopped")
      context.stop(self)
      context.system.terminate()
  }
}

object Run extends App {
  val system = ActorSystem("PongSystem")
  val pongActor = system.actorOf(Props[Pong], name = "PongActor")
}
